Python cli group chat application that persists message history in a MYSQL database.

Admin can see all online users, and can also see message history in the group chat.

I put the Admin password in the server/constants.py module

There are two packages in this application:
1.server package
2. client package 

run them  separately, and make sure you run all the DDL and DML required to setup the relational database.