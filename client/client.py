import socket
import threading
from client_helper import receive, send
from constants import *


def main():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(SERVER_ADDRESS)

    username = input("Enter a username to join chat: ")
    client.send(username.encode(FORMAT))

    if username == 'admin':
        password = input("Enter password for admin: ")
        client.send(password.encode(FORMAT))
        verify_msg = client.recv(MESSAGE_SIZE).decode(FORMAT)
        if verify_msg == 'Wrong!!':
            print("Wrong password.")
            exit()

    recv_thread = threading.Thread(target=receive, args=(client, username))
    recv_thread.start()

    write_thread = threading.Thread(target=send, args=(client,username))
    write_thread.start()


if __name__ == "__main__":
    main()
