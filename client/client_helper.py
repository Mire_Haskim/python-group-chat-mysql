from constants import *

stop_thread = False


def receive(client, username):
    while True:
        global stop_thread
        if stop_thread:
            break;
        try:
            next_message = client.recv(MESSAGE_SIZE).decode(FORMAT)
            if next_message == 'DISCONNECTED':
                print(f" You are Disconnected from the server.")
                stop_thread = True
            elif next_message == 'NOT ALLOWED':
                print("Only Admin can do this.")
            else:
                print(f"{next_message}\n")

        except:
            print("Error \n")
            client.close()
            break


def send(client, username):
    global stop_thread
    while True:
        if stop_thread:
            break
        msg = f'{input("")}'
        client.send(msg.encode(FORMAT))
