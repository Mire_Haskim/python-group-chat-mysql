import socket
import threading
from server_helper import handle_client
from constants import *


def main():
    print("Server is starting...")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(ADDRESS)
    server.listen()
    print(f"Server waiting for connections on {IP}:{PORT}")

    while True:
        connection, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(connection, addr))
        thread.start()


if __name__ == "__main__":
    main()
