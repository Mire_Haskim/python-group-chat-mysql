import mysql.connector
from database_config import DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_HOST, DATABASE_DATABASE

try:
    connection = mysql.connector.connect(user=DATABASE_USERNAME, password=DATABASE_PASSWORD,
                                         host=DATABASE_HOST, database=DATABASE_DATABASE,
                                         )

    cursor = connection.cursor()

except mysql.connector.Error as err:
    print('Database connection failed for ' + DATABASE_USERNAME + '@' + DATABASE_HOST + '/' + DATABASE_DATABASE)
    exit()


def get_messages():
    sql = "SELECT * FROM chat_history"
    results = execute(sql)
    connection.commit()
    return results


def save_message(user, message):
    sql = "INSERT INTO chat_history (User, Message) VALUES (%s, %s)"
    values = (user, message)
    cursor.execute(sql, values)
    connection.commit()


def execute(tuple, single=False, args={}, commit=False):
    cursor.execute(tuple, args)

    if commit:
        connection.commit()
    else:
        if single:
            return cursor.fetchone()
        else:
            return cursor.fetchall()


def close():
    connection.close()


def main():
    result = get_messages()
    for message in result:
        print(message)


if __name__ == "__main__":
    main()

