from threading import Thread

import constants
from database import get_messages, save_message

usernames = []
clients = []


def handle_client(conn, address):
    connected: bool = True
    username = conn.recv(constants.MESSAGE_SIZE).decode(constants.FORMAT)
    usernames.append(username)
    clients.append(conn)
    if username == 'admin':
        password = conn.recv(constants.MESSAGE_SIZE).decode(constants.FORMAT)
        if password == constants.ADMIN_PASSWORD:
            broadcast(f'Admin joined chat.'.encode(constants.FORMAT))
            print(f'Admin joined chat')

    else:
        print(f"{username} joined chat.")
        broadcast(f'{username} joined chat'.encode(constants.FORMAT))

    while connected:
        msg = conn.recv(constants.MESSAGE_SIZE).decode(constants.FORMAT)
        save_message(username, msg)
        if msg == 'list':
            if username != 'admin':
                conn.send('NOT ALLOWED'.encode(constants.FORMAT))
                continue
            else:
                for user in usernames:
                    message = f"user: {user}\n"
                    conn.send(message.encode(constants.FORMAT))

        elif msg == 'message_history':
            if username != 'admin':
                conn.send('NOT ALLOWED'.encode(constants.FORMAT))
                continue
            else:
                result = get_messages()
                for message in result:
                    # get_messages returns an array of tuples
                    print(f"{message[1]}: {message[2]}")

        elif msg == 'DISCONNECT' or msg == 'EXIT':
            conn.send("DISCONNECTED".encode(constants.FORMAT))
            print(f"{username} disconnected \n")
            broadcast(f'{username} disconnected.'.encode(constants.FORMAT))
            clients.remove(conn)
            usernames.remove(username)

            connected = False
            continue
        else:
            print(f" {username}: {msg}")
            server_msg = f"{username}: {msg}".encode(constants.FORMAT)
            broadcast(server_msg)

    conn.close()


def broadcast(message):
    for client in clients:
        client.send(message)
